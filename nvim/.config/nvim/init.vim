" source $VIMRUNTIME/defaults.vim
source $VIMRUNTIME/colors/desert.vim

" set undodir=$XDG_DATA_HOME/vim/undo
" set directory=$XDG_DATA_HOME/vim/swap
" set backupdir=$XDG_DATA_HOME/vim/backup
" set viminfo+='1000,n$XDG_DATA_HOME/vim/viminfo
" set runtimepath=$XDG_CONFIG_HOME/vim,$VIMRUNTIME,$XDG_CONFIG_HOME/vim/after

let mapleader=" "

set nocompatible
filetype plugin on
syntax on
set encoding=utf-8
set number
set relativenumber
set mouse=a

set clipboard=unnamedplus

call plug#begin()
Plug 'preservim/nerdtree'
Plug 'vimwiki/vimwiki'
Plug 'junegunn/goyo.vim'
Plug 'vim-airline/vim-airline'
Plug 'ryanoasis/vim-devicons'
Plug 'https://tildegit.org/sloum/gemini-vim-syntax'
Plug 'lifepillar/vim-mucomplete'
call plug#end()

nnoremap <C-n> :NERDTreeToggle<CR>
nnoremap <C-S-Left> <C-w><Left>
nnoremap <C-S-Right> <C-w><Right>

let g:goyo_width = 60

set guifont=LiterationMono\ Nerd\ Font\ 14

" Ortografia

nnoremap <leader>or :setlocal spell! spelllang=it,pl,en<CR>
nnoremap <leader>o? :help spell<CR>
nnoremap <leader>o- :set nospell<CR>
nnoremap <leader>on ]s			" Prossima parola sbagliata
nnoremap <leader>oN [s			" Precedente parola sbagliata
nnoremap <leader>og zg			" Segna parola come buona
nnoremap <leader>ob zw			" Segna parola come sbagliata
nnoremap <leader>os z=			" Parole sugerite

" TEXT
autocmd FileType text setlocal textwidth=78

" YAML
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" MARKDOWN
autocmd FileType markdown inoremap ** ****<esc>hi
autocmd FileType markdown inoremap * **<esc>i
autocmd FileType markdown vnoremap ** di****<esc>hPll
autocmd FileType markdown vnoremap * di**<esc>Pl

autocmd FileType markdown inoremap mdimg ![TITLE](https://)<esc>?[<CR>a
autocmd FileType markdown inoremap mdlink [TITLE](https://)<esc>?[<CR>a

