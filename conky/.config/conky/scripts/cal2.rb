def cal time, offset=0
  m = time.mon + offset
  y = time.year
  if m == 0
    m = 12
    y -= 1
  elsif m==13
    m = 1
    y += 1
  end
  `cal #{m} #{y}`
end

def aside c1,c2, s="  "
  c1 = c1.split("\n")
  c2 = c2.split("\n")
  c12 = ""
  c1.each_index{|i|
    c12 += c1[i]+s+c2[i]+"\n"
  }
  c12
end

t = Time.now
puts aside(cal(t,-1),cal(t,1))

