ipa = `ip a`.split(/^\d+: /)
ipa.each{|i|
  next if i == ""
  i = i.split("\n")
  i[0] =~ /^(\w+):/
  interface = $1
  unless [nil, "lo"].include? interface
    ip = nil
    i[1..-1].each{|l|
      if l.include? "inet "
        l =~ /(\d+)\.(\d+)\.(\d+)\.(\d+)/
        ip = [$1,$2,$3,$4].join(".")
        break
      end
    }
    next if ip == nil
    puts "#{interface}:${alignr}#{ip}"
  end
}
