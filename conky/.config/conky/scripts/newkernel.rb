yay = `yes n | yay -Ss linux | grep -E "/linux.*\\(Installed" | grep -v -E "(docs|headers|firmware)"`.chomp
# dopo mettere ":" ------------------------------------ qui ^

exit if yay == ""

uname = `uname -r`.chomp
type = ""

if uname =~ /(-[A-Za-z]+)$/
  # testato con lts e zen
  type = $1
elsif uname =~ /-arch\d+-\d+/
  type = ""
else
  # Non so che kernel è
  exit
end

yay =~ /(^|\n)\w+\/linux#{type} ([\w\.-]+) \(.*\) \(Installed:? ?(.*)\)($|\n)/

# puts type
# puts "versione del repo senza tipo:" + $2
# puts "se aggiornabile, qui la versione corrente: " + $3

unless $3 == ""
  puts "\naggiornabile al: #{$2}"
end

