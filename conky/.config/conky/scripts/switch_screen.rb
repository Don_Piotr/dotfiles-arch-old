
fn = "#{ENV["HOME"]}/.config/conky/conky.conf"

screens = `xrandr --listmonitors`.split("\n")[1..-1]
screens.map! {|l|
  l = l.split(" ")
  l[2] =~ /(\d+)\/\d+x(\d+)/
  {id: l[0][0], w: $1, h: $2}
}
m = nil
conkyconf = IO.readlines(fn)
conkyconf.map!{|l|
  if l.include? "--"
    l
  elsif l.include? "xinerama_head"
    m = l.split("=")[1].to_i
    m += 1
    if m >= screens.length
      m = 0
    end
    "    xinerama_head = #{m},"
  elsif l.include? "minimum_height"
    "    minimum_height = #{screens[m][:h]},"
  else
    l
  end
}

File.open(fn,"w"){|f|
  f.puts conkyconf
}
