
fn = "#{ENV["HOME"]}/.config/conky/conky.conf"

screens = `xrandr --listmonitors`.split("\n")[1..-1]
screens.map! {|l|
  l = l.split(" ")
  l[2] =~ /(\d+)\/\d+x(\d+)/
  {id: l[0][0], w: $1.to_i, h: $2.to_i}
}
m = `grep xinerama_head #{fn}`.split("=")[1].to_i
h = screens[m][:h]

((h-680)/2/15).times do
  puts "\n"
end
