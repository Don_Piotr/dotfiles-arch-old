# Run after login, before shell

export PATH=$PATH:$HOME/.gem/ruby/2.7.0/bin:$HOME/.scripts:$HOME/.scripts/dwm
export EDITOR=nvim
export TERMINAL=x-terminal
export BROWSER=qutebrowser

export XDG_CONFIG_HOME="$HOME/.config" XDG_CACHE_HOME="$HOME/.cache" XDG_DATA_HOME="$HOME/.local/share"
export LESSHISTFILE=-
echo "PROFILE"
